//import React, {Component} from 'react';
import Page1 from './Page1/Page1';
import Page2 from './Page2/Page2';
import Page3 from './Page3/Page3';
import Page4 from './Page4/Page4';
import Page5 from './Page5/Page5';
import Page6 from './Page6/Page6';
import Post from './Post/Post';
//import Logo from './Image/Logo';
//import Homescreen from './Homescreen/Homescreen';
import SideMenu from './SideMenu/SideMenu';
import {DrawerNavigator} from 'react-navigation';
import Loginform from './Page5/Loginform';
import Postform from './Post/Postform';
import ViewRent from './Page1/ViewRent';



export default DrawerNavigator({
  Page1: {
    screen: Page1
  },
  Page2: {
    screen: Page2
  },
  Page3: {
    screen: Page3
  },
  Page4: {
    screen: Page4
  },
  Page5: {
    screen: Page5
  },
  Page6: {
    screen: Page6
  },
  Loginform: {
    screen: Loginform
  },
  Post: {
    screen: Post
  },
  ViewRent:{
     screen: ViewRent
  },
}, {
  contentComponent: SideMenu,
  
  drawerWidth: 300
});

