import React, {Component} from 'react';
import { Text,View, Button } from 'react-native';
import Page2 from '../Page2/Page2';
import Post from '../Post/Post';
import {StackNavigator} from 'react-navigation';
import ViewRent from './ViewRent';
import ViewPost from '../Post/ViewPost';
//import {List} from 'react-native-elements';
class Page1 extends Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    return{
      headerRight: (
        <Button title="Post" color="#fff" onPress= {()=> {navigation.navigate('Post')}} />
      ),
      
    };
  }

  render () {
    return (
     <ViewRent  {...this.props} />
    );
  }
}
const Stack=StackNavigator(
  {
    Home:{
      screen:Page1,
    },
    Post: {
      screen: Post,
    }, 
    viewPost: {
      screen: ViewPost,
    }
  },

  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }

);
export default Stack;
