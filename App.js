import React, {Component} from 'react';
import DrawerNavigator from './routes';
import {AppRegistry} from 'react-native';


export default class CustomDrawer extends Component {
 
  render () {
    return (
      <DrawerNavigator/>
    );
  }
}

AppRegistry.registerComponent('CustomDrawer', () => CustomDrawer);
