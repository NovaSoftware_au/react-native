/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Component} from 'react';
import styles from './SideMenu.style';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View} from 'react-native';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <Text style={styles.sectionHeadingStyle}>
              在线信息
            </Text>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page1')}>
              租房信息
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page2')}>
              车辆交易
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page3')}>
              二手消息
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page6')}>
              生活服务
              </Text>
            </View>
          </View>
          <View>
            <Text style={styles.sectionHeadingStyle}>
              个人信息
            </Text>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page5')}>
                登陆/注册
              </Text>
            </View>
          </View>

          <View>
            <Text style={styles.sectionHeadingStyle}>
            Section 3
            </Text>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page4')}>
                关于我们
              </Text>
            </View>  
          </View>



        </ScrollView>
        <View style={styles.footerContainer}>
          <Text>This is my fixed footer</Text>
        </View>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;
