import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, Image, Button, ScrollView, CameraRoll} from 'react-native';
import { ListItem } from 'react-native-elements';
import Divider from 'react-native-divider';




class Postform extends Component {
  constructor(props){
    super(props)

    this.state={
      userid: '1',
      username: '',
      content: '',
      numLike:'0',
      category:'',
      idSHOP:'null',
      dateTime:'',
      idQUICKORDER:'null',
      posting: false,
      urlImages:'null',
      imageNameStore:[],
      files: [],
      mode: 'simple', 
      title:'', 
      photos: []

    }
  }

  handleButtonPress = () => {
    CameraRoll.getPhotos({
        first: 100,
        assetType: 'Photos',
      })
      .then(r => {
        this.setState({ photos: r.edges });
      })
      .catch((err) => {
         //Error Loading Images
      });
    };

    submitContent = () => {
      this.setState(() => {
        fetch('http://pte.novasoftware.com.au:3090/newPost')
      })
    }



    render () {
      return (       
        <View style={styles.container}>  
            <View style={styles.TitleContainer }>
            <Text style={alignItems='center'}>发布消息</Text>
            </View>
            
            <View style={styles.contentContainer}>
            <TextInput
              style={{height: 40, width:330, borderColor: 'gray', borderWidth: 1, marginBottom: 10,}}
              placeholder="Pleae enter title hesre"
              onChangeText={(title) =>this.setState({title})}
              value={this.state.title}
              /> 
              
            <TextInput
              style={{height: 160, width:330, borderColor: 'gray', borderWidth: 1, marginBottom: 10,}}
              placeholder="Please enter content here"
              onChangeText={(content) =>this.setState({content})}
              value={this.state.content}
              />
            <Divider>上传图片</Divider>
            <Button
              title="add pic"
              color='#6f6f6f'
              onPress={this.handleButtonPress}
            />
            
            <ScrollView>
                  {this.state.photos.map((a, i) => {
                  return (
                    <Image
                      key={i}
                      style={{
                        width: 100,
                        height: 100,
                      }}
                      source={{ uri: a.node.image.uri }}
                    />
                  );
                })}
                </ScrollView>

                <Button
                  title="提交"
                  color='#6f6f6f'
                  onPress={this.submitContent}
                />
          </View>
        </View>
  
      );
    }
  }
  
  export default Postform;

  const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',


    },
    contentContainer:{
        padding: 20,
        

    },
    TitleContainer:{
      
        fontSize: 16,
        textAlign: 'center',
        color: '#6f6f6f',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    UploadPicContainer:{
      
      fontSize: 16,
      textAlign: 'left',
      color: '#6f6f6f',
      paddingLeft:15,
      
    }
})
